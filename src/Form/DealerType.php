<?php

namespace App\Form;

use App\Entity\Dealer;
use App\Entity\Brand;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
class DealerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('geositeId')
            ->add('brands',EntityType::class, [
    // looks for choices from this entity
    'class' => Brand::class,

    // uses the User.username property as the visible option string
    'choice_label' => 'name',

    // used to render a select box, check boxes or radios
    'multiple' => true,
    // 'expanded' => true,
])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Dealer::class,
        ]);
    }
}
