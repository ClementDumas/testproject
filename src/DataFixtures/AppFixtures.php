<?php

namespace App\DataFixtures;

use App\Entity\Brand;
use App\Entity\Dealer;
use App\Entity\Engine;
use App\Entity\Fuel;
use App\Entity\Owner;
use App\Entity\Vehicle;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;


class AppFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {

        $faker = Faker\Factory::create('fr_FR');
        $owners = [];
        $brands = [];
        $fuels = [];
        $dealers = [];
        $engines = [];


        //BRAND
        for ($i = 0; $i < 20; $i++) {
            $brand = new Brand();
            $brand->setName($faker->word);
            $brand->setCode(strtoupper($faker->word));
            $brands[]= $brand;
            $manager->persist($brand);
        }
        //DEALER
        for ($i = 0; $i < 20; $i++) {
            $dealer = new Dealer();
            $dealer->setGeositeId(23+$i);
            $dealers[]=$dealer;
            $manager->persist($dealer);
        }


        //FUEL
        for ($i = 0; $i < 20; $i++) {
            $fuel = new Fuel();
            $fuel->setType($faker->word);
            $fuels[]= $fuel;
            $manager->persist($fuel);
        }

        //ENGINE
        for ($i = 0; $i < 20; $i++) {
            $engine = new Engine();
            $engine->setType($faker->word);
            $engine->setFuel($fuels[array_rand($fuels,1)]);
            $engines[]= $engine;
            $manager->persist($engine);
        }

        //OWNER
        for ($i = 0; $i < 20; $i++) {
            $owner = new Owner();
            $owner->setName($faker->name);
            $owner->setFirstname($faker->name);
            $owner->setMail($faker->email);

            $owners[]=$owner;
            $manager->persist($owner);
        }

        //VEHICLES
        for ($i = 0; $i < 20; $i++) {
            $car = new Vehicle();
            $car->setName($faker->word);
            $car->setVin($i+3);
            $car->setNbWheels(random_int(0,8));
            $car->setColor($faker->colorName);

            $rand = rand(0,1);
            if($rand==0) {
                $car->setOwner($owners[array_rand($owners, 1)]);
            }
            $car->setBrand($brands[array_rand($brands,1)]);
            $car->setEngine($engines[array_rand($engines,1)]);
            $car->setDealer($dealers[array_rand($dealers)]);
            $manager->persist($car);
        }

        $manager->flush();
    }
}
