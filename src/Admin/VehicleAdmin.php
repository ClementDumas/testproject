<?php


namespace App\Admin;
use App\Entity\Brand;
use App\Entity\Dealer;
use App\Entity\Engine;
use App\Entity\Owner;
use App\Form\BrandType;
use App\Form\DealerType;
use App\Form\EngineType;
use App\Form\OwnerType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class VehicleAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', TextType::class);
        $formMapper->add('vin', TextType::class);
        $formMapper->add('nbWheels', TextType::class);
        $formMapper->add('color', TextType::class);
        $formMapper->add('owner', ModelType::class, [
                'class' => Owner::class,
                'property' => 'name',
            ])
        ;

        $formMapper->add('brand', EntityType::class, [
            'class' => Brand::class,
            'choice_label' => 'name',
        ]  );
        $formMapper->add('engine', EntityType::class, [
            'class' => Engine::class,
            'choice_label' => 'type',
        ]  );
        $formMapper->add('dealer', EntityType::class, [
            'class' => Dealer::class,
            'choice_label' => 'geositeId',
        ]  );





    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
        $listMapper->addIdentifier('vin');
        $listMapper->addIdentifier('owner');

    }
}