<?php

namespace App\Entity;

use App\Service\VehicleOwnershipHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VehicleRepository")
 * @UniqueEntity("vin")
 */
class Vehicle
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $vin;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbWheels;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $color;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Owner", inversedBy="vehicles")
     */
    private $owner;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Brand", inversedBy="vehicles")
     * @Assert\NotNull()
     */
    private $brand;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Engine", inversedBy="vehicles")
     * @Assert\NotNull()
     */
    private $engine;



    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Dealer", inversedBy="vehicles")
     */
    private $dealer;

    /**
     * @ORM\Column(type="string", length=255, columnDefinition="ENUM('secondHand','new')")
     * @Assert\Choice({"secondHand", "new"})
     */
    private $category;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VehicleOwnership", mappedBy="vehicle", cascade={"persist"})
     */
    private $vehicleOwnerships;

    public function __construct()
    {
        $this->vehicleOwnerships = new ArrayCollection();
        $this->category='new';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getVin(): ?string
    {
        return $this->vin;
    }

    public function setVin(string $vin): self
    {
        $this->vin = $vin;

        return $this;
    }

    public function getNbWheels(): ?int
    {
        return $this->nbWheels;
    }

    public function setNbWheels(int $nbWheels): self
    {
        $this->nbWheels = $nbWheels;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

   

    public function getOwner(): ?Owner
    {
        return $this->owner;
    }

    public function setOwner(?Owner $owner): self
    {
        $vehicleHelper = new VehicleOwnershipHelper;
        if($owner) {
            $vehicleHelper->setPastOwner($this, $owner);
        }
        $vehicleHelper->setSecondHand($this);
        $this->owner = $owner;
        return $this;
    }

    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    public function setBrand(?Brand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getEngine(): ?Engine
    {
        return $this->engine;
    }

    public function setEngine(?Engine $engine): self
    {
        $this->engine = $engine;

        return $this;
    }


    public function getDealer(): ?Dealer
    {
        return $this->dealer;
    }

    public function setDealer(?Dealer $dealer): self
    {
        $this->dealer = $dealer;
        $this->dealer->addBrand($this->brand);


        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }



    /**
     * @return Collection|VehicleOwnership[]
     */
    public function getVehicleOwnerships(): Collection
    {
        return $this->vehicleOwnerships;
    }

    public function addVehicleOwnership(VehicleOwnership $vehicleOwnership): self
    {
        if (!$this->vehicleOwnerships->contains($vehicleOwnership)) {
            $this->vehicleOwnerships[] = $vehicleOwnership;
            $vehicleOwnership->setVehicle($this);
        }

        return $this;
    }

    public function removeVehicleOwnership(VehicleOwnership $vehicleOwnership): self
    {
        if ($this->vehicleOwnerships->contains($vehicleOwnership)) {
            $this->vehicleOwnerships->removeElement($vehicleOwnership);
            // set the owning side to null (unless already changed)
            if ($vehicleOwnership->getVehicle() === $this) {
                $vehicleOwnership->setVehicle(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->name;
    }
}
