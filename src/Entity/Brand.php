<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BrandRepository")
 */
class Brand
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Vehicle", mappedBy="brand")
     */
    private $vehicles;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Dealer", mappedBy="brands")
     */
    private $dealer;

    public function __construct()
    {
        $this->vehicles = new ArrayCollection();
        $this->dealer = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Collection|Vehicle[]
     */
    public function getVehicles(): Collection
    {
        return $this->vehicles;
    }

    public function addVehicle(Vehicle $vehicle): self
    {
        if (!$this->vehicles->contains($vehicle)) {
            $this->vehicles[] = $vehicle;
            $vehicle->setBrand($this);
        }

        return $this;
    }

    public function removeVehicle(Vehicle $vehicle): self
    {
        if ($this->vehicles->contains($vehicle)) {
            $this->vehicles->removeElement($vehicle);
            // set the owning side to null (unless already changed)
            if ($vehicle->getBrand() === $this) {
                $vehicle->setBrand(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Dealer[]
     */
    public function getDealer(): Collection
    {
        return $this->dealer;
    }

    public function addDealer(Dealer $dealer): self
    {
        if (!$this->dealer->contains($dealer)) {
            $this->dealer[] = $dealer;
            $dealer->addBrand($this);
        }

        return $this;
    }

    public function removeDealer(Dealer $dealer): self
    {
        if ($this->dealer->contains($dealer)) {
            $this->dealer->removeElement($dealer);
            $dealer->removeBrand($this);
        }


        return $this;
    }

    public function __toString()
      {
        return $this->name;
      }
}
