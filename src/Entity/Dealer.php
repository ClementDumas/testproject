<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DealerRepository")
 * @UniqueEntity("geositeId")
 */
class Dealer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", unique=true)
     */
    private $geositeId;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Vehicle", mappedBy="dealer")
     */
    private $vehicles;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Brand", inversedBy="dealer")
     * @ORM\JoinTable(name="dealer_brand",joinColumns={@ORM\JoinColumn(name="dealer_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="brand_id",referencedColumnName="id")})
     */
    private $brands;

    public function __construct()
    {
        $this->vehicles = new ArrayCollection();
        $this->brands = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGeositeId(): ?int
    {
        return $this->geositeId;
    }

    public function setGeositeId(int $geositeId): self
    {
        $this->geositeId = $geositeId;

        return $this;
    }

    /**
     * @return Collection|Vehicle[]
     */
    public function getVehicles(): Collection
    {
        return $this->vehicles;
    }

    public function addVehicle(Vehicle $vehicle): self
    {
        if (!$this->vehicles->contains($vehicle)) {

            $this->vehicles[] = $vehicle;
            $vehicle->setDealer($this);
        }

        return $this;
    }

    public function removeVehicle(Vehicle $vehicle): self
    {
        if ($this->vehicles->contains($vehicle)) {
            $this->vehicles->removeElement($vehicle);
            // set the owning side to null (unless already changed)
            if ($vehicle->getDealer() === $this) {
                $vehicle->setDealer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Brand[]
     */
    public function getBrands(): Collection
    {
        return $this->brands;
    }

    public function addBrand(Brand $brand): self
    {

        $brand->addDealer($this);
        if (!$this->brands->contains($brand)) {
            $this->brands[] = $brand;
            $brand->addDealer($this);
        }

        return $this;
    }

    public function removeBrand(Brand $brand): self
    {
        if ($this->brands->contains($brand)) {
            $this->brands->removeElement($brand);
            $brand->removeDealer($this);
        }

        return $this;
    }
    public function __toString()
      {
        return strval($this->geositeId);
      }
}
