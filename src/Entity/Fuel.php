<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FuelRepository")
 */
class Fuel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Engine", mappedBy="fuel")
     */
    private $engine;

    public function __construct()
    {
        $this->engine = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Engine[]
     */
    public function getEngine(): Collection
    {
        return $this->engine;
    }

    public function addEngine(Engine $engine): self
    {
        if (!$this->engine->contains($engine)) {
            $this->engine[] = $engine;
            $engine->setFuel($this);
        }

        return $this;
    }

    public function removeEngine(Engine $engine): self
    {
        if ($this->engine->contains($engine)) {
            $this->engine->removeElement($engine);
            // set the owning side to null (unless already changed)
            if ($engine->getFuel() === $this) {
                $engine->setFuel(null);
            }
        }

        return $this;
    }
    public function __toString()
      {
        return $this->type;
      }

}
