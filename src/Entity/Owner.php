<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OwnerRepository")
 */
class Owner
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     *@Assert\Email()
     */
    private $mail;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;


    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Media", cascade={"persist", "remove"})
     */
    private $avatar;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Vehicle", mappedBy="owner")
     */
    private $vehicles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VehicleOwnership", mappedBy="owner")
     */
    private $vehicleOwnerships;

    public function __construct()
    {
        $this->vehicles = new ArrayCollection();
        $this->vehicleOwnerships = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return Collection|Vehicle[]
     */
    public function getVehicles(): Collection
    {
        return $this->vehicles;
    }

    public function addVehicle(Vehicle $vehicle): self
    {
        if (!$this->vehicles->contains($vehicle)) {
            $this->vehicles[] = $vehicle;
            $vehicle->setOwner($this);
        }

        return $this;
    }

    public function removeVehicle(Vehicle $vehicle): self
    {
        if ($this->vehicles->contains($vehicle)) {
            $this->vehicles->removeElement($vehicle);
            // set the owning side to null (unless already changed)
            if ($vehicle->getOwner() === $this) {
                $vehicle->setOwner(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->name;
    }

    /**
     * @return Collection|VehicleOwnership[]
     */
    public function getVehicleOwnerships(): Collection
    {
        return $this->vehicleOwnerships;
    }

    public function addVehicleOwnership(VehicleOwnership $vehicleOwnership): self
    {
        if (!$this->vehicleOwnerships->contains($vehicleOwnership)) {
            $this->vehicleOwnerships[] = $vehicleOwnership;
            $vehicleOwnership->setOwner($this);
        }

        return $this;
    }

    public function removeVehicleOwnership(VehicleOwnership $vehicleOwnership): self
    {
        if ($this->vehicleOwnerships->contains($vehicleOwnership)) {
            $this->vehicleOwnerships->removeElement($vehicleOwnership);
            // set the owning side to null (unless already changed)
            if ($vehicleOwnership->getOwner() === $this) {
                $vehicleOwnership->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAvatar(): ?Media
    {
        return $this->avatar;
    }

    /**
     * @param mixed $avatar
     */
    public function setAvatar(?Media $avatar): self
    {
        $this->avatar = $avatar;
        return $this;
    }


}
