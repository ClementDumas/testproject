<?php

namespace App\Controller;

use App\Entity\Dealer;
use App\Form\DealerType;
use App\Repository\DealerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/dealer")
 */
class DealerController extends AbstractController
{
    /**
     * @Route("/", name="dealer_index", methods={"GET"})
     */
    public function index(DealerRepository $dealerRepository): Response
    {
        return $this->render('dealer/index.html.twig', [
            'dealers' => $dealerRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="dealer_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $dealer = new Dealer();
        $form = $this->createForm(DealerType::class, $dealer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($dealer);
            $entityManager->flush();

            return $this->redirectToRoute('dealer_index');
        }

        return $this->render('dealer/new.html.twig', [
            'dealer' => $dealer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="dealer_show", methods={"GET"})
     */
    public function show(Dealer $dealer): Response
    {
        return $this->render('dealer/show.html.twig', [
            'dealer' => $dealer,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="dealer_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Dealer $dealer): Response
    {
        $form = $this->createForm(DealerType::class, $dealer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('dealer_index');
        }

        return $this->render('dealer/edit.html.twig', [
            'dealer' => $dealer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="dealer_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Dealer $dealer): Response
    {
        if ($this->isCsrfTokenValid('delete'.$dealer->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($dealer);
            $entityManager->flush();
        }

        return $this->redirectToRoute('dealer_index');
    }
}
