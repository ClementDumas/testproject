<?php

namespace App\Controller;

use App\Entity\VehicleOwnership;
use App\Form\VehicleOwnershipType;
use App\Repository\VehicleOwnershipRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ownership")
 */
class VehicleOwnershipController extends AbstractController
{
    /**
     * @Route("/", name="vehicle_ownership_index", methods={"GET"})
     */
    public function index(VehicleOwnershipRepository $vehicleOwnershipRepository): Response
    {
        return $this->render('vehicle_ownership/index.html.twig', [
            'vehicle_ownerships' => $vehicleOwnershipRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="vehicle_ownership_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $vehicleOwnership = new VehicleOwnership();
        $form = $this->createForm(VehicleOwnershipType::class, $vehicleOwnership);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($vehicleOwnership);
            $entityManager->flush();

            return $this->redirectToRoute('vehicle_ownership_index');
        }

        return $this->render('vehicle_ownership/new.html.twig', [
            'vehicle_ownership' => $vehicleOwnership,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="vehicle_ownership_show", methods={"GET"})
     */
    public function show(VehicleOwnership $vehicleOwnership): Response
    {
        return $this->render('vehicle_ownership/show.html.twig', [
            'vehicle_ownership' => $vehicleOwnership,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="vehicle_ownership_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, VehicleOwnership $vehicleOwnership): Response
    {
        $form = $this->createForm(VehicleOwnershipType::class, $vehicleOwnership);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('vehicle_ownership_index');
        }

        return $this->render('vehicle_ownership/edit.html.twig', [
            'vehicle_ownership' => $vehicleOwnership,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="vehicle_ownership_delete", methods={"DELETE"})
     */
    public function delete(Request $request, VehicleOwnership $vehicleOwnership): Response
    {
        if ($this->isCsrfTokenValid('delete'.$vehicleOwnership->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($vehicleOwnership);
            $entityManager->flush();
        }

        return $this->redirectToRoute('vehicle_ownership_index');
    }
}
