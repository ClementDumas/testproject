<?php

namespace App\Repository;

use App\Entity\VehicleOwnership;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method VehicleOwnership|null find($id, $lockMode = null, $lockVersion = null)
 * @method VehicleOwnership|null findOneBy(array $criteria, array $orderBy = null)
 * @method VehicleOwnership[]    findAll()
 * @method VehicleOwnership[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VehicleOwnershipRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VehicleOwnership::class);
    }

    // /**
    //  * @return VehicleOwnership[] Returns an array of VehicleOwnership objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VehicleOwnership
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
