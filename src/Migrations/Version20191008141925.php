<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191008141925 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE vehicle CHANGE owner_id owner_id INT DEFAULT NULL, CHANGE brand_id brand_id INT DEFAULT NULL, CHANGE engine_id engine_id INT DEFAULT NULL, CHANGE dealer_id dealer_id INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1B80E486B1085141 ON vehicle (vin)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_17A33902A45FC8BB ON dealer (geosite_id)');
        $this->addSql('ALTER TABLE engine CHANGE fuel_id fuel_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_17A33902A45FC8BB ON dealer');
        $this->addSql('ALTER TABLE engine CHANGE fuel_id fuel_id INT DEFAULT NULL');
        $this->addSql('DROP INDEX UNIQ_1B80E486B1085141 ON vehicle');
        $this->addSql('ALTER TABLE vehicle CHANGE owner_id owner_id INT DEFAULT NULL, CHANGE brand_id brand_id INT DEFAULT NULL, CHANGE engine_id engine_id INT DEFAULT NULL, CHANGE dealer_id dealer_id INT DEFAULT NULL');
    }
}
