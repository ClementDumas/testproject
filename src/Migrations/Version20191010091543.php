<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191010091543 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE vehicle_ownership CHANGE owner_id owner_id INT DEFAULT NULL, CHANGE vehicle_id vehicle_id INT DEFAULT NULL, CHANGE end_date end_date DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE engine CHANGE fuel_id fuel_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE vehicle CHANGE owner_id owner_id INT DEFAULT NULL, CHANGE brand_id brand_id INT DEFAULT NULL, CHANGE engine_id engine_id INT DEFAULT NULL, CHANGE dealer_id dealer_id INT DEFAULT NULL, CHANGE category category ENUM(\'secondHand\',\'new\')');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE engine CHANGE fuel_id fuel_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE vehicle CHANGE owner_id owner_id INT DEFAULT NULL, CHANGE brand_id brand_id INT DEFAULT NULL, CHANGE engine_id engine_id INT DEFAULT NULL, CHANGE dealer_id dealer_id INT DEFAULT NULL, CHANGE category category VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE vehicle_ownership CHANGE owner_id owner_id INT DEFAULT NULL, CHANGE vehicle_id vehicle_id INT DEFAULT NULL, CHANGE end_date end_date DATE NOT NULL');
    }
}
