<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191009090338 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE vehicle_ownership (id INT AUTO_INCREMENT NOT NULL, owner_id INT DEFAULT NULL, vehicle_id INT DEFAULT NULL, comment VARCHAR(255) NOT NULL, INDEX IDX_EF2FA9C67E3C61F9 (owner_id), INDEX IDX_EF2FA9C6545317D1 (vehicle_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE vehicle_ownership ADD CONSTRAINT FK_EF2FA9C67E3C61F9 FOREIGN KEY (owner_id) REFERENCES owner (id)');
        $this->addSql('ALTER TABLE vehicle_ownership ADD CONSTRAINT FK_EF2FA9C6545317D1 FOREIGN KEY (vehicle_id) REFERENCES vehicle (id)');
        $this->addSql('ALTER TABLE engine CHANGE fuel_id fuel_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE vehicle CHANGE owner_id owner_id INT DEFAULT NULL, CHANGE brand_id brand_id INT DEFAULT NULL, CHANGE engine_id engine_id INT DEFAULT NULL, CHANGE dealer_id dealer_id INT DEFAULT NULL, CHANGE category category ENUM(\'secondHand\',\'new\')');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE vehicle_ownership');
        $this->addSql('ALTER TABLE engine CHANGE fuel_id fuel_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE vehicle CHANGE owner_id owner_id INT DEFAULT NULL, CHANGE brand_id brand_id INT DEFAULT NULL, CHANGE engine_id engine_id INT DEFAULT NULL, CHANGE dealer_id dealer_id INT DEFAULT NULL, CHANGE category category VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
    }
}
