<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191008154736 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE dealer_brand (dealer_id INT NOT NULL, brand_id INT NOT NULL, INDEX IDX_31246D26249E6EA1 (dealer_id), INDEX IDX_31246D2644F5D008 (brand_id), PRIMARY KEY(dealer_id, brand_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE dealer_brand ADD CONSTRAINT FK_31246D26249E6EA1 FOREIGN KEY (dealer_id) REFERENCES dealer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dealer_brand ADD CONSTRAINT FK_31246D2644F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE brand_dealer');
        $this->addSql('ALTER TABLE engine CHANGE fuel_id fuel_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE vehicle CHANGE owner_id owner_id INT DEFAULT NULL, CHANGE brand_id brand_id INT DEFAULT NULL, CHANGE engine_id engine_id INT DEFAULT NULL, CHANGE dealer_id dealer_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE brand_dealer (brand_id INT NOT NULL, dealer_id INT NOT NULL, INDEX IDX_942B450C249E6EA1 (dealer_id), INDEX IDX_942B450C44F5D008 (brand_id), PRIMARY KEY(brand_id, dealer_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE brand_dealer ADD CONSTRAINT FK_942B450C249E6EA1 FOREIGN KEY (dealer_id) REFERENCES dealer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE brand_dealer ADD CONSTRAINT FK_942B450C44F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE dealer_brand');
        $this->addSql('ALTER TABLE engine CHANGE fuel_id fuel_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE vehicle CHANGE owner_id owner_id INT DEFAULT NULL, CHANGE brand_id brand_id INT DEFAULT NULL, CHANGE engine_id engine_id INT DEFAULT NULL, CHANGE dealer_id dealer_id INT DEFAULT NULL');
    }
}
