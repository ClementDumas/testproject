<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191008141127 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE brand_dealer (brand_id INT NOT NULL, dealer_id INT NOT NULL, INDEX IDX_942B450C44F5D008 (brand_id), INDEX IDX_942B450C249E6EA1 (dealer_id), PRIMARY KEY(brand_id, dealer_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE brand_dealer ADD CONSTRAINT FK_942B450C44F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE brand_dealer ADD CONSTRAINT FK_942B450C249E6EA1 FOREIGN KEY (dealer_id) REFERENCES dealer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE engine ADD fuel_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE engine ADD CONSTRAINT FK_E8A81A8D97C79677 FOREIGN KEY (fuel_id) REFERENCES fuel (id)');
        $this->addSql('CREATE INDEX IDX_E8A81A8D97C79677 ON engine (fuel_id)');
        $this->addSql('ALTER TABLE vehicle ADD owner_id INT DEFAULT NULL, ADD brand_id INT DEFAULT NULL, ADD engine_id INT DEFAULT NULL, ADD dealer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E4867E3C61F9 FOREIGN KEY (owner_id) REFERENCES owner (id)');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E48644F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id)');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E486E78C9C0A FOREIGN KEY (engine_id) REFERENCES engine (id)');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E486249E6EA1 FOREIGN KEY (dealer_id) REFERENCES dealer (id)');
        $this->addSql('CREATE INDEX IDX_1B80E4867E3C61F9 ON vehicle (owner_id)');
        $this->addSql('CREATE INDEX IDX_1B80E48644F5D008 ON vehicle (brand_id)');
        $this->addSql('CREATE INDEX IDX_1B80E486E78C9C0A ON vehicle (engine_id)');
        $this->addSql('CREATE INDEX IDX_1B80E486249E6EA1 ON vehicle (dealer_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE brand_dealer');
        $this->addSql('ALTER TABLE engine DROP FOREIGN KEY FK_E8A81A8D97C79677');
        $this->addSql('DROP INDEX IDX_E8A81A8D97C79677 ON engine');
        $this->addSql('ALTER TABLE engine DROP fuel_id');
        $this->addSql('ALTER TABLE vehicle DROP FOREIGN KEY FK_1B80E4867E3C61F9');
        $this->addSql('ALTER TABLE vehicle DROP FOREIGN KEY FK_1B80E48644F5D008');
        $this->addSql('ALTER TABLE vehicle DROP FOREIGN KEY FK_1B80E486E78C9C0A');
        $this->addSql('ALTER TABLE vehicle DROP FOREIGN KEY FK_1B80E486249E6EA1');
        $this->addSql('DROP INDEX IDX_1B80E4867E3C61F9 ON vehicle');
        $this->addSql('DROP INDEX IDX_1B80E48644F5D008 ON vehicle');
        $this->addSql('DROP INDEX IDX_1B80E486E78C9C0A ON vehicle');
        $this->addSql('DROP INDEX IDX_1B80E486249E6EA1 ON vehicle');
        $this->addSql('ALTER TABLE vehicle DROP owner_id, DROP brand_id, DROP engine_id, DROP dealer_id');
    }
}
