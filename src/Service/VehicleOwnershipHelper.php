<?php


namespace App\Service;


use App\Entity\Owner;
use App\Entity\Vehicle;
use App\Entity\VehicleOwnership;

class VehicleOwnershipHelper
{
    public function setPastOwner(Vehicle $vehicle, Owner $owner){
        $pastOwns = $vehicle->getVehicleOwnerships();
        $length = count($pastOwns);
        if($length>0){
            $pastOwns[$length-1]->setIsActive(false);
            $pastOwns[$length-1]->setEndDate(new \DateTime());
        }
        $vOwn = new VehicleOwnership($owner, $vehicle);
        $vOwn->setIsActive(true);
        $vehicle->addVehicleOwnership($vOwn);

    }

    public  function setSecondHand(Vehicle $vehicle){
        if(!empty($vehicle->getVehicleOwnerships())){
            $vehicle->setCategory('secondHand');
        }
    }

}